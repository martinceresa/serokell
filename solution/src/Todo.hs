module Todo
  ( TodoListM
  , runTodoList
  ) where

import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.State.Strict (StateT, get, gets, modify, put, runStateT)
import qualified Data.ByteString.Char8 as B
import Data.Char (isLetter, toLower)
import Data.Function (on)
import Data.List (intersect)
import Data.List.NonEmpty (toList)

--------------------------------------------------------------------------------
import qualified Data.Map.Strict as M
import Types (Description (..)
             , Index (..)
             , MonadTodoList (..)
             , SearchParams (..)
             , SearchWord
             , Tag (..)
             , TaskState (..)
             , TodoItem (..)
             -- Lens
             , getDescription
             , getIndex
             , getSearchWord
             , getTag
             , tiDescription
             , tiTags
             --
             )
--
-- Lens
import Control.Lens hiding (Index)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Default Definitions
initialIndex :: Index
initialIndex = Index 0

initialItem :: Index -> Description -> [Tag] -> TodoItem
initialItem i desc ts = TodoItem i desc ts

--------------------------------------------------------------------------------
-- Helper function

-- | Given a |TaskState| gives us back |True| if it is marked as done.
taskDone :: TaskState -> Bool
taskDone Done = True
taskDone Todo = False

toLowerByte :: B.ByteString -> B.ByteString
toLowerByte = B.map toLower

--------------------------------------------------------------------------------
type Task = (TodoItem, TaskState)

data TodoList = TodoList { _todoList      :: M.Map Index Task
                         , _todoIdCounter :: Index }
  deriving (Show, Eq)
makeLenses ''TodoList

newTask :: Index -> Description -> [Tag] -> Task
newTask i desc tgs = (initialItem i desc tgs, Todo)

emptyTodoList :: TodoList
emptyTodoList = TodoList M.empty initialIndex

newtype TodoListM a = TodoListM { runTodoListM :: StateT TodoList IO a }
  deriving (Functor, Applicative, Monad, MonadIO)

runTodoList :: TodoListM a -> IO ()
runTodoList = void . flip runStateT emptyTodoList . runTodoListM

--------------------------------------------------------------------------------
-- | |tickIndex| Adds 1 to the index counter
tickIndex :: TodoList -> TodoList
tickIndex = over ( todoIdCounter . getIndex ) (+ 1)

-- | |addTodo| adds a new Todo-Item to a given |TodoList|
addTodo :: Description -> [Tag] -> TodoList -> TodoList
addTodo d ts tList = over todoList (M.insert idIndex (newTask idIndex d ts)) tList
  where idIndex = view todoIdCounter tList

-- | |doneTask| marks a given |Index| as done.
-- It is said that no |done N| command can precede adding the item with the
-- number |N| but it is safer to assume that it might and handle such case in the
-- monad instance later.
doneTask :: Index -> TodoList -> Maybe TodoList
doneTask i s = case M.lookup i (view todoList s) of
                 (Just task) ->
                   Just (over todoList (M.insert i (_2 .~ Done $ task)) s)
                 _ -> Nothing

-- | |subList| checks that all elements of the
-- first list are elements of the second list.
subList :: [Tag] -> [Tag] -> Bool
subList ls rs = all (flip elem rs') ls'
  where
    -- | Making everything lower case. Clearly this could be improved
    ls' = map (over getTag toLowerByte) ls
    rs' = map (over getTag toLowerByte) rs

subWord :: SearchWord -> Description -> Bool
subWord w = (B.isInfixOf (w ^. getSearchWord)) . (view getDescription)

-- | |searchWords| checks that all words are in
-- a given description.
searchWords :: [SearchWord] -> Description -> Bool
searchWords sWs desc = flip all sWs' $ flip subWord desc'
  where
    -- | Some note as before, this could be improved.
    sWs' = map (over getSearchWord toLowerByte) sWs
    desc' = over getDescription toLowerByte desc

-- | |searchTasks| filters a |TodoList| following the words and tags given
searchTasks :: [SearchWord] -> [Tag] -> TodoList -> TodoList
searchTasks swds ts = over todoList (
           M.filter (\tItem -> -- | Given a task
                       -- | check that is not marked as done
                        (not $ taskDone $ tItem ^. _2)
                       -- | That contains all the given tags
                        && subList ts (tItem ^. _1 . tiTags )
                       -- | and all words in its description
                        && searchWords swds (tItem ^. _1 . tiDescription)
                    )
          )

-- | Get the |Index| counter from the State
getCounter :: StateT TodoList IO Index
getCounter = gets (view todoIdCounter)
--------------------------------------------------------------------------------

instance MonadTodoList TodoListM where
  add  desc tags = TodoListM $
                          modify (addTodo desc tags)
                          >> getCounter
                          >>= \c -> modify tickIndex >> return c
  -- | Mark a task as |Done|, failing if there is no such task
  done idTask = TodoListM $ get >>= maybe (fail "Error") put . doneTask idTask
  search params = TodoListM $
    -- | get the state
    get
    -- | return the elements
    >>= return . M.elems
    -- | after filtered for the |params| inside the state
    . M.map (view _1) . view todoList . searchTasks (spWords params) (spTags params)

module Types
  ( Index (..)
  , SearchWord (..)
  , Description (..)
  , getDescription
  , Tag (..)
  , SearchParams (..)
  , TodoItem (..)
  , TaskState (..)
  -- Lenses
  , tiIndex, tiDescription, tiTags , getIndex
  , getTag
  , getSearchWord
  --
  , MonadTodoList (..)
  ) where

import Data.ByteString.Char8 (ByteString)
import Data.List.NonEmpty (NonEmpty)

-- Lens
import Control.Lens.TH
import Control.Lens hiding (Index)

newtype Index = Index { _getIndex :: Word }
  deriving (Show, Eq, Enum, Bounded, Ord)
makeLenses ''Index

newtype Description = Description { _getDescription :: ByteString }
  deriving (Show, Eq, Ord)
makeLenses ''Description

newtype Tag = Tag { _getTag :: ByteString }
  deriving (Show, Eq, Ord)
makeLenses ''Tag

newtype SearchWord = SearchWord { _getSearchWord :: ByteString }
  deriving (Show, Eq, Ord)
makeLenses ''SearchWord

data SearchParams = SearchParams
  { spWords :: ![SearchWord]
  , spTags  :: ![Tag]
  } deriving (Show, Eq)

data TodoItem = TodoItem
  { _tiIndex       :: !Index
  , _tiDescription :: !Description
  , _tiTags        :: ![Tag]
  } deriving (Show, Eq, Ord)
makeLenses ''TodoItem

data TaskState = Done | Todo
  deriving (Show, Eq, Ord)

class MonadTodoList m where
  add    :: Description -> [Tag] -> m Index
  done   :: Index -> m ()
  search :: SearchParams -> m [TodoItem]
